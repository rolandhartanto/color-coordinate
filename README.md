# Color Coordinate
Computer Graphics Assignment 5  
19M38254 - Roland Hartanto

The source code of the implementation is in the [<code>assignment5.ipynb</code>](https://gitlab.com/rolandhartanto/color-coordinate/blob/master/assignment5.ipynb) file.  
Sometimes, the figures cannot be seen via Github/Gitlab. In such case, refer to [<code>notebook.pdf</code>](https://gitlab.com/rolandhartanto/color-coordinate/blob/master/notebook.pdf) to view the complete version.

To access the original question of this assignment, refer to [<code>question.pdf</code>](https://gitlab.com/rolandhartanto/color-coordinate/blob/master/question.pdf).  

Data:  
- Pigmentdata folder contains the pigments spectral reflectances data.
- XYZ_CIE_2.dat.txt is the matching function of CIEXYZ 1931.
